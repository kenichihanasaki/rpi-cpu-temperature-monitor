CFLAGS=-Wall -O3 -g
OBJECTS=main.o
BINARIES=rpi-cpu-temperature-monitor
LDFLAGS=-lrt -lm -lpthread

all : $(BINARIES)

main.o: main.c

rpi-cpu-temperature-monitor : $(OBJECTS)
	gcc $(CFLAGS) $^ -o $@ $(LDFLAGS)

clean:
	rm -f $(OBJECTS) $(BINARIES)
