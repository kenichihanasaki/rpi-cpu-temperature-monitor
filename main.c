/* 
 * File:   main.c
 * Author: system
 *
 * Created on 2015/04/29, 13:17
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>

#define BUFFER_SIZE (512)
#define INTERVAL (3)

volatile bool g_running = true;

/**
 * 
 */
static void signalHandler(int sig)
{
	g_running = false;
}

/**
 * 
 */
int main(int argc, char** argv) {
	
	int max_temperature = 80 * 1000;
	int temperature = 0;
	
	char command[BUFFER_SIZE];
	char buffer[BUFFER_SIZE];
	
	const char* path = "/sys/class/thermal/thermal_zone0/temp";
    
    FILE* fp = NULL;
	
	if (SIG_ERR == signal(SIGINT, signalHandler)) {
		exit(EXIT_FAILURE);
	}
	
	if(argc >= 2)
	{
		temperature = atoi(argv[1]) * 1000;
		
		if(temperature != 0)
		{
			max_temperature = temperature;
		}
	}
	
	memset(command, 0, sizeof(command));
	
	if(argc >= 3)
	{
		strncpy(command, argv[2], BUFFER_SIZE - 1);
	}
	else
	{
		strncpy(command, "sudo halt", BUFFER_SIZE - 1);
	}
	
	//printf("max_temperature=%d\n", max_temperature);
	
	while(g_running)
	{
		if((fp = fopen(path, "r")) != NULL)
		{
			fgets(buffer, sizeof(buffer), fp);
			fclose(fp);
			fp = NULL;
			
			temperature = atoi(buffer);
			
			if(temperature >= max_temperature)
			{
				system(command);
				break;
			}
		}
		
		sleep(INTERVAL);
	}
    
	if(fp != NULL)
	{
		fclose(fp);
	}
	
	return (EXIT_SUCCESS);
}

